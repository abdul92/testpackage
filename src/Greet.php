<?php

namespace Alphadev\Greet;

class Greet
{
    public function greet(String $name)
    {
        return "Hello {$name}! How are you doing today?";
    }
}